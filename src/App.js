import './App.css';
import Login from "./components/Login/Login";
import Profile from "./components/Profile/Profile";
import {BrowserRouter, Switch} from "react-router-dom";
import AppRoutes from "./consts/AppRoutes"
import PublicRoute from "./hoc/PublicRoute"
import Translation from "./components/Translation/Translation";
import PrivateRoute from "./hoc/PrivateRoute";


function App() {
  return (

    <BrowserRouter>
        <div className="App">
            <Switch>
                <PublicRoute path={ AppRoutes.Login } exact component={ Login }/>
                <PrivateRoute path={ AppRoutes.Profile } component={ Profile }/>
                <PrivateRoute path={AppRoutes.Translation} component={ Translation }/>
            </Switch>
        </div>
    </BrowserRouter>


);
}

export default App;
