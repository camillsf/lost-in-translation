/***
 * existing paths
 */
export const AppRoutes = {
    Login: '/',
    Profile: '/profile',
    Translation: '/translation'
}
export default AppRoutes
