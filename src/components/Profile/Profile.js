import React from 'react'
import { useState, useEffect } from 'react'
import {getStorage} from "../../utils/localStorage";
import { useHistory } from "react-router-dom";



function Profile () {

    const history = useHistory()

    /**
     * Setting user
     * */
    const [user, setUser] = useState('')

    /**
     * Setting translations
     * */
    const [translations, setTranslations] = useState([])

    /**
     * Setting numOfTranslations
     * */
    const [numOfTranslations, setNumOfTranslations] = useState('')

    /**
     * If the user clicks the log out button. LocalsStorage will be cleared
     * and the login page is rendered
     * */
    const clearLocalStorage = () => {
        localStorage.clear();
        history.push('/')
    }

    /**
     * This useEffect hook will get the local storage and store it in the states. The states are used to display
     * the last up tp 10 translations of the user.
     * The deps is set to [] to only execute once after rendering the component
     */
    useEffect(() => {
        const storage = getStorage('ra_session')

        if(storage) {
            setUser(storage.session.user)
            setTranslations(storage.session.translations)
            setNumOfTranslations(storage.session.translations.length)
            console.log(storage.session.translations)
        }
    }, [])


    /**
     * If the user clicks on the robot in the upper left corner,
     * this function will be called, and the translation page is rendered.
     * */
    const goToTranslation = () => {
        history.push('/translation')
    }

    return (
        <>
            <nav className="navbarStyle">
                <ul className="pullLeft">
                    <li className="navLi">
                        <div className="layeredImage" style={{ background: `url(${process.env.PUBLIC_URL}/Splash.svg)`, backgroundRepeat:"no-repeat",
                            backgroundSize: "60px" }}>
                            <img className="imageNavbar" alt="robot" src={`/Logo.png`} onClick={goToTranslation}/>
                        </div>
                    </li>
                    <li className="navLi">
                        <h2 className="navbarText">Lost in translation</h2>
                    </li>
                </ul>
                <ul className="pullRight">
                    <p className="userName">{ user }</p>
                    <button className="logOutButton" type="button" onClick={clearLocalStorage}> Log out </button>
                </ul>
            </nav>
            <section className="sectionProfile">
                <table className="tableProfile">
                    <th className="textProfile">Your last {numOfTranslations} translations:</th>
                    {translations.map(translation => (
                        <tr className="translationList" key={translation}> {translation} </tr>
                    ))}
                </table>

            </section>
        </>
    )
}

export default Profile;