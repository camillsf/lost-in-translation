import React from 'react';
import {useState} from "react";
import {getStorage, setStorage} from "../../utils/localStorage";
import { useHistory } from "react-router-dom"


function Translation() {
    const history = useHistory()

    /**
     * Setting translation
     * **/
    const [translation, setTranslation] = useState({
        translation: ''
        }
    )

    /**
     * Setting images
     * **/
    const [images, setImages] = useState([])


    /**
     * When input changes, we firstly check if the input is more
     * than 40 characters, if it is we alert the user and return.
     * If it's not we update the translation to be the new input.
     * **/
    const inputChange = e => {
        if(e.target.value.length > 40){
            window.alert("to many chars")
            return
        }
        setTranslation(
            e.target.value
        )
    }


    /**
     * This method pushes the current translation to storage.
     * If the storage already has 10 translations we remove the
     * first(oldest) element from the translations storage before
     * adding the new translation to it.
     * **/
    const pushToStorage = () => {
        let storage = getStorage('ra_session')

        if(storage.session.translations.length === 10){
            setStorage('ra_session', {
                session: {
                    user: storage.session.user,
                    translations: storage.session.translations.filter((e, index)=>(index !== 0))
                }
            })
            storage = getStorage('ra_session')
        }
        let translations = [...storage.session.translations, translation]

        setStorage('ra_session', {
            session: {
                user: storage.session.user,
                translations: translations
            }
        })
    }


    /**
     * This method is called when the translate-button is clicked.
     * It firstly calls the pushToStorage() - method. Then We transform
     * the translation text into an array of lowerCase characters.
     * The images gets set by mapping over the translationArray.
     * Each letter in the translationArray is transformed into an image tag
     * referring to the signed-image of the letter. And each space is
     * transformed into a break tag.
     * **/
    const handleTranslation = () => {
        pushToStorage()

        const translationArray = [...translation.toLowerCase()]

        setImages(translationArray.map((imageName, index) => {
             if(imageName !== " ") {
                 return <img className="responseImg"
                     key={index}
                     src={`/individial_signs/${imageName}.png`}
                     alt={imageName}/>
             }
             else{
                 return <br/>
             }
        }));
    }


    /**
     * When the profile button is clicked, we render the profile page.
     * **/
    const goToProfile = () => {
        history.push('/profile')
    }


    return (
        <>
            <nav className="navbarStyle">
                <ul className="pullLeft">
                    <li className="navLi">
                        <div className="layeredImage" style={{ background: `url(${process.env.PUBLIC_URL}/Splash.svg)`, backgroundRepeat:"no-repeat",
                            backgroundSize: "60px" }}>
                            <img className="imageNavbar" alt="robot" src={`/Logo.png`} />
                        </div>
                    </li>
                    <li className="navLi">
                        <h2 className="navbarText">Lost in translation</h2>
                    </li>
                </ul>
                <ul className="pullRight">
                    <li className="navLi">
                        <p className="userName">{getStorage('ra_session').session.user}</p>
                        <button type="button" id="profileButton" className="material-icons" onClick={goToProfile}>person</button>
                    </li>
                </ul>
            </nav>
            <form className="formTranslation">
                <fieldset className="translationFormSection, fieldsetStyleTranslation">
                    <span className="material-icons-outlined" id="keyboard">keyboard</span>
                    <span id="line"> | </span>
                    <input className="inputStyle" type="text" id="translation" onChange={ inputChange } placeholder="What do you want to translate?"/>
                    <button className="material-icons" type="button" onClick={ handleTranslation } id="login-button">arrow_forward</button>
                </fieldset>
            </form>
            <section className="sectionTranslation">
                <div className="divOutputBox">
                    { images }
                    <div className="divBottomTranslation">
                        <p className="bottomText">
                            Translation
                        </p>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Translation;