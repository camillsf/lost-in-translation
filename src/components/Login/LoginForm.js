import React, {useState} from "react";


function LoginForm( props ) {
    /**
     * Setting username
     * **/
    const [username, setUsername] = useState({
        username: ''
    })


    /**
     * When input changes, we update the username to be the new input.
     * **/
    const inputChange = e => {
        setUsername(
             e.target.value
        )
    }


    /**
     * When login-button is clicked, we firstly check if the
     * username is filled in, if not, we alert the user and return.
     * If it is we send the username to the complete-prop, so that
     * the parents method onLoginComplete is executed.
     * **/
    const onClickLogin = () => {
        if(username.username === '') {
            window.alert("please enter your name")
            return
        }
        else {
            props.complete(username)
        }
    }


    return (
        <form className="loginFormInput">
            <fieldset className="loginFormSection, fieldsetLogin">
                <span className="material-icons-outlined" id="keyboard">keyboard</span>
                <span id="line"> | </span>
                <input className="inputStyle" type="text" id="username" onChange={ inputChange} placeholder="What's your name?"/>
                <button className="material-icons" type="button" onClick={ onClickLogin } id="login-button">arrow_forward</button>
            </fieldset>
            <div className="divBottomLogin">
            </div>
        </form>
    )
}

export default LoginForm;