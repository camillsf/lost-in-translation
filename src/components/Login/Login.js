import React from 'react';
import LoginForm from "./LoginForm"
import {setStorage} from "../../utils/localStorage";
import { useHistory } from "react-router-dom"


function Login(){
    const history = useHistory()

    /**
     * When the loginForm is completed the localStorage sets the
     * user to the result from the loginForm which is the username.
     * Then it renders the translation page.
     * **/
    const onLoginComplete = result =>  {
        setStorage('ra_session', {
            session: {
                user: result,
                translations: []
            }
        })
        history.push('/translation')
    }


    return (
        <>
            <nav className="navbarStyle">
                <ul className="pullLeft">
                    <li className="navLi">
                        <h2 className="navbarText">Lost in translation</h2>
                    </li>
                </ul>
            </nav>
            <section className="loginSection">
                <div className="layeredImage" style={{ background: `url(${process.env.PUBLIC_URL}/Splash.svg)`, backgroundRepeat:"no-repeat",
                    backgroundSize: "200px" }}>
                    <img className="loginImg" alt="robot" src={`/Logo.png`} />
                </div>
                <div className="headerLogin">
                    <h1 className="h1Login">Lost in Translation</h1>
                    <p className="textLogin">Get started</p>
                </div>
            </section>

            <section className="loginFormSection">
                < LoginForm complete={ onLoginComplete } />
            </section>
        </>
    )

}

export default Login;